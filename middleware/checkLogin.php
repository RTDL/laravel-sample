<?php

namespace App\Http\Middleware;

use Closure;

class checkLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (empty($request->session()->get('user'))) {
            return response()->json(['unauthorized' => 'user could not be authenticated'], 401);
        }
        return $next($request);
    }
}
