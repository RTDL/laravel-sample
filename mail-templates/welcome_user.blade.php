
@include('emails/layout/email-header')

<!-- Body Start -->
<tr style="border-collapse:collapse;"> 
	<td align="left" style="padding:0;Margin:0;padding-bottom:15px;"> 
		<h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:Roboto !important;font-size:16px;font-style:normal;font-weight:normal;color:#333333;">
			Hello <b>{{ucwords($receiver_name)}}, </b> &nbsp;
		</h2>
	</td> 
</tr>
<tr style="border-collapse:collapse;"> 
	<td align="left" style="padding:0;Margin:0;"> 
		<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Roboto !important;line-height:21px;color:#333333;">
			<b> 
				We are welcome you on Sample. <br>
			</b>
		</p>
	</td> 
</tr>
<!-- Body End -->

@include('emails/layout/email-footer')