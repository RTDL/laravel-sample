
@include('emails/layout/email-header')

<!-- Body Start -->
<tr style="border-collapse:collapse;"> 
	<td align="left" style="padding:0;Margin:0;padding-bottom:15px;"> 
		<h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:Roboto !important;font-size:16px;font-style:normal;font-weight:normal;color:#333333;">
			Hello <b>{{ucwords($receiver_name)}}, </b> &nbsp;
		</h2>
	</td> 
</tr>
<tr style="border-collapse:collapse;"> 
	<td align="left" style="padding:0;Margin:0;"> 
		<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Roboto !important;line-height:21px;color:#333333;">
			<b> 
				You successfully registered on Sample. <br>
				To activate your account, Please click on Activate button below or copy the link and open it in your browser.
			</b>
		</p>
	</td> 
</tr>
<tr style="border-collapse:collapse;"> 
	<td align="center" style="padding:10px;Margin:0;"> <span class="es-button-border" style="border-style:solid;border-color:#FB2729;background:#FB2729;border-width:0px 0px 2px 0px;display:inline-block;border-radius:30px;width:auto;"> <a href="{{env('APP_URL')}}activate/{{$body_message['link']}}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none !important;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FB2729;border-width:10px 20px 10px 20px;display:inline-block;background:#FB2729;border-radius:30px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">Activate Account</a> </span> </td> 
</tr>
<!-- Body End -->

@include('emails/layout/email-footer')