<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['web']], function () {
	Route::post('register','registerController@create');
	Route::post('login','registerController@login');
	Route::post('/logout','registerController@logout');
	Route::post('forgot-password','registerController@forgotPassword');
	Route::post('/setPassword','registerController@setNewPassword');
	Route::post('/verify/passwordToken', 'registerController@verifyPasswordToken');
	Route::post('google', 'registerController@socialLogin');
	Route::post('facebook', 'registerController@socialLogin');
});