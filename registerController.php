<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\commonTrait;
use App\User as user;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Socialite;
use App;
class registerController extends Controller {
	use commonTrait;

	public function create(Request $request) {
		// Captcha Verification Code
		$recaptchaToken = $request->handleToken;
		if($this->recaptcha($recaptchaToken) == false)
			return response()->json(['error' => "Invalid Data."], 400);
		
		$validator = Validator::make($request->all(), [
			'fullName' => 'required|string|max:255',
			'email' => 'required|string|email|max:255',
			'password' => 'required|string|min:8|max:16|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,16}$"'
		]);
		$errors = $validator->errors();
		if (count($errors)>0) {
			for ($i=0; $i <count($errors) ; $i++) { 
				return response()->json(['error' => $validator->errors()->all()[$i]], 400);
			}
		}
			
		$user = user::where(['email' => strtolower($request->email),'isDeleted' => 0])->first();
		if (!empty($user)) {
			if ($user->isBlocked == 1)
				return response()->json(['error' => "Your account is suspended, Please contact Sample team."], 400);
			else
				return response()->json(['error' => 'Email Id already exists.'], 400);
		}

		$request->activationToken = Crypt::encryptString(time().rand(1111111111,9999999999));
		$user = user::select()->where('email',strtolower($request->email))->where('isDeleted', 1)->first();
		if (!empty($user)) {
			user::where('email', strtolower($request->email))->update(['isDeleted' => 0, 'activationToken' => $request->activationToken,'password'=>md5($request->password)]);

			$registerMail = $this->sendMail($layout='emails.register', strtolower($request->email), $request->fullName,"Sample: Activate your account",['link'=>urldecode($request->activationToken)]);

			if ($registerMail) {
				return response()->json(['clientMessage' => 'Register Successfully, Please check your email inbox to activate your account.'], 200);
			}	
		}
		$store = new user;
		$request->zone = $this->timezone();
		$result = $store->store($request);
		
		$registerMail = $this->sendMail($layout='emails.register', strtolower($request->email), $request->fullName,"Sample: Activate your account",['link'=>urldecode($request->activationToken)]);
		if ($registerMail) {
			return response()->json(['clientMessage' => 'Register onces Successfully, Please check your email inbox to activate your account.'], 200);
		}
		
	}

	public function activeToken($token) {
		$user = user::where('activationToken', '=', $token)->first();
		if(empty($user))
			return redirect('/login?status=500');
		$active = user::where('id', '=', $user->id)->update(['isActive'=>1, 'activationToken' => '']);
		if ($active){
			$this->sendMail($layout='emails.activation_user', strtolower($user->email), $user->fullName,"Sample: Activation account",[]);
			return redirect('/login?status=200');
		} 
		return redirect('/login?status=500');
	}

	public function login(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|string|email|max:255',
			'password' => 'required|string|min:8|max:16|regex:"^.{8,16}$"'
		]);

		$errors = $validator->errors();

		if (count($errors)>0) {
			return response()->json(['error' => 'Invalid Username or Password.'], 400);
		}

		$userDetails = user::where('email', '=', strtolower($request->email))
			->where('password','=',md5($request->password))
			->where('isDeleted','=',0)
			->where('isBlocked','=',0)
			->where('isActive','=',1)
			->first();

		if (!empty($userDetails)) {
			$userDetails['UID'] = md5(rand(111,999).time());
			$request->session()->put('user',$userDetails);
			return response()->json(['user' => $userDetails], 200);
		}
		else{
			return response()->json(['error' => 'Invalid Email or Password.'], 400);
		}
	}

	public function logout(Request $request) {
		$request->session()->flush();
		return response()->json(['status' => 200], 200);
	}

	public function forgotPassword(Request $request) {
		$user = user::where(['email'=>strtolower($request->email),'isDeleted'=>0])->first();
		if ($user['email'] == strtolower($request->email)) {
			$activationToken = Crypt::encryptString(time().rand(1111111111,9999999999));

			user::where('email', '=', strtolower($request->email))->update(['resetPasswordToken' => $activationToken]);

			$registerMail = $this->sendMail($layout='emails.forgotPassword', strtolower($request->email),$user['fullName'],"Sample: Forgot Password",['link'=>urlencode($activationToken)]);

			return response()->json(['clientMessage' => "Email sent, Check your email for a message from the Sample team."], 200);
		}
		else {
			return response()->json(['clientMessage' => "Email sent, Check your email for a message from the Sample team."], 200);
		}
	}

	public function verifyPasswordToken(Request $request) {
		$user = user::where('resetPasswordToken', '=', $request->_token)->first();
		if (!empty($user)) {
			return response()->json(array('user' => $user), 200);
		}
		else {
			return response()->json(['error' => 'Token is expired !!'], 400);
		}
	}

	public function setNewPassword(Request $request) {
		$validator = Validator::make($request->all(), [
			'password' => 'required|string|min:8|max:16|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,16}$"'
		]);

		$errors = $validator->errors();

		if (count($errors)>0) {
			return response()->json(['error' => 'Invalid Password.'], 400);
		}

		$userDetails = user::where('resetPasswordToken', '=', $request->token)->first();
		if (!empty($userDetails)) {
			if ($userDetails->isActive == 0) {
				$user = user::where(['resetPasswordToken'=>$request->token])->update(['isActive'=>1,'activationToken'=>'']);
			}
			$user = user::where('resetPasswordToken', '=', $request->token)->update(['password' => md5($request->password), 'resetPasswordToken' => '']);
			$registerMail = $this->sendMail($layout='emails.changePassword', $userDetails['email'],$userDetails['fullName'],"Sample: Password Changed",[]);
			return response()->json(['status' => true], 200);
		}
		else {
			return response()->json(['error' => 'Token for reset password is expired.'], 400);
		}
	}

	/*******************************************
	* Users Login via Google & Facebook
	********************************************/
	public function socialLogin(Request $request) {
		if (empty($request->ReceiveToken))
			return response()->json(['error' => 'Token is expired !!'], 400);

		if($request->Provider == "GOOGLE"){
			$user = Socialite::driver('google')->userFromToken($request->ReceiveToken);
			$data = $user->getName();
			$email = $user->getEmail();
			$provider = $request->Provider;
			$userData = user::where(['email' => strtolower($email),'isDeleted' => 0])->first();
			
			if (!empty($userData)) {
				if ($userData->isBlocked == 1)
					return response()->json(['error' => "Your account is suspended, Please contact Sample team."], 400);
				$userData = user::select()->where('email',strtolower($email))->first();
				if($userData->provider_name == $provider){
					$request->session()->put('user',$userData);
					return response()->json(['user' => $data], 200);
				}
				else
					return response()->json(['error' => 'Email Id already exists.'], 400);
			}
		}
		if($request->Provider == "FACEBOOK"){
			$user = Socialite::driver('facebook')->userFromToken($request->ReceiveToken);
			$data = $user->getName();
			$email = $user->getEmail();
			$provider = $request->Provider;
			$userData = user::where(['email' => strtolower($email),'isDeleted' => 0])->first();
			
			if (!empty($userData)) {
				if ($userData->isBlocked == 1)
					return response()->json(['error' => "Your account is suspended, Please contact Sample team."], 400);
				else{
		
					$userData = user::select()->where('email',strtolower($email))->first();
					if($userData->provider_name == $provider){
						$request->session()->put('user',$userData);
						return response()->json(['user' => $data], 200);
					}
					else
						return response()->json(['error' => 'Email Id already exists.'], 400);
				}
			}
		}
		
		$userData = user::select()->where('email',strtolower($email))->where('isDeleted', 1)->first();
		$obj = new user;
		$obj->fullName = $data;
		$obj->email = $email;
		$obj->password = null;
		$obj->provider_name = $provider;
		$obj->activationToken = null;
		$obj->isActive = 1;
		$obj->zone = $this->timezone();
		$store = new user;
		$result = $store->store($obj);
		$request->session()->put('user',$result);
		
		$this->sendMail($layout='emails.welcome_user', strtolower($obj->email), $obj->fullName," Sample: Welcome You ",[]);
		$this->sendMail($layout='emails.activation_user', strtolower($obj->email), $obj->fullName,"Sample: Activation account",[]);
		return response()->json(['user' => $data], 200);
	}
 
	/*******************************************
	* Users Login TimeZone get function
	********************************************/
	public function timezone() {
		if (App::environment('local')){
			$ip = "Asia/Kolkata";
			return $ip;
		}
		else{
			$ip = $_SERVER['REMOTE_ADDR'];
			$url = "http://ip-api.com/json/$ip?fields=status,message,country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,query";
			$client = new \GuzzleHttp\Client();
			$response = $client->request('GET', $url);
			$json = $response->getBody();
			$zone = json_decode($json);
			$zoneTime = $zone->timezone;
			return $zoneTime;
		}
	}
}